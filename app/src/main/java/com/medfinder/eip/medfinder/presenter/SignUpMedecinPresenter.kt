package com.medfinder.eip.medfinder.presenter

import android.util.Log
import com.medfinder.eip.medfinder.model.SignUpMedecinInteractor
import com.medfinder.eip.medfinder.ui.viewContract.SignUpView

class SignUpMedecinPresenter(private var signUpView: SignUpView?, private val signUpMedecinInteractor: SignUpMedecinInteractor)
    : SignUpMedecinInteractor.OnFinishedListener {
    fun sendIDtoInteractor(userMail: String, userPass: String, userName: String, userRefNumber: String) {
        signUpMedecinInteractor.sendSignUpRequest(this, userMail, userPass, userName, userRefNumber)
    }

    // TODO: RAJOUTER LE TOKEN EN PARAMETRE
    override fun onResultSuccess() {
        signUpView?.goToLogin()
    }

    override fun onResultFailure(errorMessage: String) {
        signUpView?.showError(errorMessage)
        Log.d("SIGNUPMEDECIN", "CA MARCHE PO")
    }

    fun onDestroy() {
        signUpView = null
    }
}