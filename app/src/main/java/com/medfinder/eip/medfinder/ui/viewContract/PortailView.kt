package com.medfinder.eip.medfinder.ui.viewContract

interface PortailView {
    fun goToPatientSignUp()
    fun goToMedecinSignUp()
}