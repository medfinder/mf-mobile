package com.medfinder.eip.medfinder.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.medfinder.eip.medfinder.R
import com.medfinder.eip.medfinder.model.SignUpMedecinInteractor
import com.medfinder.eip.medfinder.presenter.SignUpMedecinPresenter
import com.medfinder.eip.medfinder.ui.viewContract.SignUpView

class SignUpMedecinActivity : AppCompatActivity(), SignUpView {

    private lateinit var signUpMedecinPresenter : SignUpMedecinPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUpMedecinPresenter = SignUpMedecinPresenter(this, SignUpMedecinInteractor(this))
        setContentView(R.layout.activity_register_medecin)
    }

    override fun onStart() {
        super.onStart()

        val userName = findViewById<EditText>(R.id.name)
        val userMail = findViewById<EditText>(R.id.mail)
        val userPass = findViewById<EditText>(R.id.mdp)
        val userConfPass = findViewById<EditText>(R.id.mdpConf)
        val userRefNumber = findViewById<EditText>(R.id.numRef)
        val inscriptionButton = findViewById<Button>(R.id.connexionButton)

        inscriptionButton.setOnClickListener {
            Log.d("SIGNUPMEDECIN", userName.text.toString())
            Log.d("SIGNUPMEDECIN", userMail.text.toString())
            Log.d("SIGNUPMEDECIN", userPass.text.toString())
            Log.d("SIGNUPMEDECIN", userConfPass.text.toString())
            Log.d("SIGNUPMEDECIN", userRefNumber.text.toString())

            if (userConfPass.text.toString() != userPass.text.toString())
                showError("The passwords does not match")
            else
                signUpMedecinPresenter.sendIDtoInteractor(
                    userMail.text.toString(),
                    userPass.text.toString(),
                    userName.text.toString(),
                    userRefNumber.text.toString())
        }
    }
    override fun showError(errorMessage: String?) {
        this@SignUpMedecinActivity.runOnUiThread {
            val toast = Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG)
            toast.show()
        }
    }

    override fun goToLogin() {
        val intent = Intent(this@SignUpMedecinActivity, MainActivity::class.java)
        finish()
        startActivity(intent)
    }

    override fun onDestroy() {
        signUpMedecinPresenter.onDestroy()
        super.onDestroy()
    }
}
