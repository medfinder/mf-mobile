package com.medfinder.eip.medfinder.presenter

import android.util.Log
import com.medfinder.eip.medfinder.model.LoginInteractor
import com.medfinder.eip.medfinder.ui.viewContract.LoginView

class LoginPresenter(private var loginView: LoginView?, private val loginInteractor: LoginInteractor)
    : LoginInteractor.OnFinishedListener {

    fun sendIDtoInteractor(userMail: String, userPass: String) {
        loginInteractor.sendConnexionRequest(this, userMail, userPass)
    }

    override fun onResultSuccess(username: String, userToken: String, userPass: String) {
        loginView?.fillUserPref(username, userPass, userToken)
        loginView?.goToHome(username, userToken)
    }

    override fun onResultFailure(errorMessage: String) {
        loginView?.showError(errorMessage)
        Log.d("LOGIN", "CA MARCHE PO")
    }

    fun onDestroy() {
        loginView = null
    }
}