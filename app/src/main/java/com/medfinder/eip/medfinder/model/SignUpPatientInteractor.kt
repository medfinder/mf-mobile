package com.medfinder.eip.medfinder.model

import android.content.Context
import android.util.Log
import com.medfinder.eip.medfinder.Util
import okhttp3.*
import org.json.JSONObject
import java.io.IOException


class SignUpPatientInteractor(val context: Context) {
    private var okhttpclient = OkHttpClient()
    private lateinit var appContext: Context

    val JSON = MediaType.parse("application/json; charset=utf-8")

    interface OnFinishedListener {
        fun onResultSuccess()
        fun onResultFailure(errorMessage: String)
    }

    fun sendSignUpRequest(
        onFinishedListener: OnFinishedListener
        , userMail: String
        , userPass: String
        , userName: String
    ) {

        appContext = context
        val url = "${Util.getProperty("host", context)}/api/signup/users"

        val json = JSONObject()
            .put("username", userName)
            .put("password", userPass)
            .put("email", userMail)
        val requestBody = RequestBody.create(JSON, json.toString())


        val request = Request.Builder()
            .addHeader("Content-Type", "application/json")
            .url(url)
            .post(requestBody)
            .build()

        okhttpclient.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val data = response.body()?.string()
                if (data != "Bad Request") {
                    val jsonObj = JSONObject(data)
                    Log.d("SIGNUPPATIENT", "reponse $jsonObj")
                    if (jsonObj.get("success") != true)
                        onFinishedListener.onResultFailure(jsonObj.getString("reason"))
                    else
                        onFinishedListener.onResultSuccess()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                Log.d("onFailure", "SIGNUPPATIENT " + e.message)
                onFinishedListener.onResultFailure("There was an error during the inscription")
            }
        })
    }
}