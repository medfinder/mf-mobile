package com.medfinder.eip.medfinder.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import com.medfinder.eip.medfinder.R
import com.medfinder.eip.medfinder.ui.viewContract.PortailView

class PortailActivity : AppCompatActivity(), PortailView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_portail)
    }

    override fun onStart() {
        super.onStart()

        val patientButton = findViewById<ImageButton>(R.id.patient_button)
        val medecinButton = findViewById<ImageButton>(R.id.medecin_button)

        patientButton.setOnClickListener {
            goToPatientSignUp()
        }

        medecinButton.setOnClickListener {
            goToMedecinSignUp()
        }
    }

    override fun goToPatientSignUp() {
        val intent = Intent(this@PortailActivity, SignUpPatientActivity::class.java)
        intent.putExtra("user_type", false)
        finish()
        startActivity(intent)
    }

    override fun goToMedecinSignUp() {
        val intent = Intent(this@PortailActivity, SignUpMedecinActivity::class.java)
        intent.putExtra("user_type", true)
        finish()
        startActivity(intent)
    }
}
