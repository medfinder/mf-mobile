package com.medfinder.eip.medfinder.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import com.medfinder.eip.medfinder.R

class HomePageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)
    }

    override fun onStart() {
        super.onStart()

        val glossaryButton = findViewById<Button>(R.id.glossaireAccess)

        glossaryButton.setOnClickListener {
            Log.d("HOME_ACTIVIY", "pressed glossary access button")
            val intent = Intent(this@HomePageActivity, GlossarySearchActivity::class.java)
            startActivity(intent)
        }
    }
}
