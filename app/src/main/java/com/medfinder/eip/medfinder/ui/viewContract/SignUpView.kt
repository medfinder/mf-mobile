package com.medfinder.eip.medfinder.ui.viewContract

interface SignUpView {
    fun goToLogin()
    fun showError(errorMessage: String?)
//    fun fillUserPref(userMail: String, userPass: String)
}