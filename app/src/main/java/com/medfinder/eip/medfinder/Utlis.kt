package com.medfinder.eip.medfinder

import android.content.Context
import android.content.res.AssetManager
import java.io.IOException
import java.io.InputStream
import java.util.Properties

object Util {
    @Throws(IOException::class)
    fun getProperty(key: String, context: Context): String {
        val properties = Properties()
        val assetManager = context.assets
        val inputStream = assetManager.open("medfinder.properties")
        properties.load(inputStream)
        return properties.getProperty(key)
    }
}