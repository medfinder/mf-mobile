package com.medfinder.eip.medfinder.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.medfinder.eip.medfinder.R
import com.medfinder.eip.medfinder.model.SignUpPatientInteractor
import com.medfinder.eip.medfinder.presenter.SignUpPatientPresenter
import com.medfinder.eip.medfinder.ui.viewContract.SignUpView

class SignUpPatientActivity : AppCompatActivity(), SignUpView {

    private lateinit var signUpPatientPresenter : SignUpPatientPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUpPatientPresenter = SignUpPatientPresenter(this, SignUpPatientInteractor(this))
        setContentView(R.layout.activity_register_patient)
    }

    override fun onStart() {
        super.onStart()

        val userName = findViewById<EditText>(R.id.name)
        val userMail = findViewById<EditText>(R.id.mail)
        val userPass = findViewById<EditText>(R.id.mdp)
        val userConfPass = findViewById<EditText>(R.id.mdpConf)
        val inscriptionButton = findViewById<Button>(R.id.connexionButton)

        inscriptionButton.setOnClickListener {
            Log.d("SIGNUPMEDECIN", userName.text.toString())
            Log.d("SIGNUPMEDECIN", userMail.text.toString())
            Log.d("SIGNUPMEDECIN", userPass.text.toString())
            Log.d("SIGNUPMEDECIN", userConfPass.text.toString())

            if (userConfPass.text.toString() != userPass.text.toString())
                showError("The passwords does not match")
            else
                signUpPatientPresenter.sendIDtoInteractor(
                    userMail.text.toString(),
                    userPass.text.toString(),
                    userName.text.toString())
        }
    }
    override fun showError(errorMessage: String?) {
        this@SignUpPatientActivity.runOnUiThread {
            val toast = Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG)
            toast.show()
        }
    }

    override fun goToLogin() {
        val intent = Intent(this@SignUpPatientActivity, MainActivity::class.java)
        finish()
        startActivity(intent)
    }

    override fun onDestroy() {
        signUpPatientPresenter.onDestroy()
        super.onDestroy()
    }
}
