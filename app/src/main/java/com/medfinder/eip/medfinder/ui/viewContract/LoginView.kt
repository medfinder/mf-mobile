package com.medfinder.eip.medfinder.ui.viewContract

interface LoginView {
    fun goToHome(username: String, userToken: String)
    fun showError(errorMessage: String?)
    fun getSessionID()
    fun goToPortal()
    fun fillUserPref(userMail: String, userPass: String, userToken: String)
}