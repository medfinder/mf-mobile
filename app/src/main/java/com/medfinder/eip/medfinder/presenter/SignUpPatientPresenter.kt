package com.medfinder.eip.medfinder.presenter

import android.util.Log
import com.medfinder.eip.medfinder.model.SignUpPatientInteractor
import com.medfinder.eip.medfinder.ui.viewContract.SignUpView


class SignUpPatientPresenter(private var signUpView: SignUpView?, private val signUpPatientInteractor: SignUpPatientInteractor)
    : SignUpPatientInteractor.OnFinishedListener {
    fun sendIDtoInteractor(userMail: String, userPass: String, userName: String) {
        signUpPatientInteractor.sendSignUpRequest(this, userMail, userPass, userName)
    }

    override fun onResultSuccess() {
        signUpView?.goToLogin()
    }

    override fun onResultFailure(errorMessage: String) {
        signUpView?.showError(errorMessage)
        Log.d("SIGNUPMEDECIN", "CA MARCHE PO")
    }

    fun onDestroy() {
        signUpView = null
    }
}