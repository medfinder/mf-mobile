package com.medfinder.eip.medfinder.model

import android.content.Context
import android.content.res.Resources
import android.util.Log
import com.medfinder.eip.medfinder.R
import com.medfinder.eip.medfinder.Util
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class SignUpMedecinInteractor(val context: Context) {
    private var okhttpclient = OkHttpClient()
    val JSON = MediaType.parse("application/json; charset=utf-8")
    private lateinit var appContext : Context

    interface OnFinishedListener {
        fun onResultSuccess()
        fun onResultFailure(errorMessage: String)
    }

    fun sendSignUpRequest(onFinishedListener: OnFinishedListener
                          , userMail: String
                          , userPass: String
                          , userName: String,
                          userRefNumber: String) {

        appContext = context
        val url = "${Util.getProperty("host", appContext)}/api/signup/medic"
        val json = JSONObject()
            .put("username", userName)
            .put("password", userPass)
            .put("email", userMail)
            .put("reference", userRefNumber.toInt())

        val requestBody = RequestBody.create(JSON, json.toString())


        val request = Request.Builder()
            .addHeader("Content-Type", "application/json")
            .url(url)
            .post(requestBody)
            .build()

        okhttpclient.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val data = response.body()?.string()
                if (data != "Bad Request") {
                    val jsonObj = JSONObject(data)
                    Log.d("SIGNUPMEDECIN", "reponse $jsonObj")
                    if (jsonObj.get("success") != true)
                        onFinishedListener.onResultFailure(jsonObj.getString("reason"))
                    else
                        onFinishedListener.onResultSuccess()
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                Log.d("onFailure", "SIGNUPMEDECIN " + e.message)
                onFinishedListener.onResultFailure("There was an error during the inscription")
            }
        })
    }
}