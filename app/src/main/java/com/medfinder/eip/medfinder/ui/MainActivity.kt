package com.medfinder.eip.medfinder.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.medfinder.eip.medfinder.R
import com.medfinder.eip.medfinder.model.LoginInteractor
import com.medfinder.eip.medfinder.presenter.LoginPresenter
import com.medfinder.eip.medfinder.ui.viewContract.LoginView

class MainActivity : AppCompatActivity(), LoginView {

    private lateinit var loginPresenter: LoginPresenter
    private lateinit var userPrefs : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        loginPresenter = LoginPresenter(this, LoginInteractor(this))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getSessionID()
    }

    override fun onStart() {
        super.onStart()
        val eMailInput = findViewById<EditText>(R.id.mailInput)
        val passInput = findViewById<EditText>(R.id.passwordInput)
        val connexionButton = findViewById<Button>(R.id.connexionButton)
        val signUpCTA = findViewById<TextView>(R.id.inscriptionLink)

        connexionButton.setOnClickListener {
            Log.d(CONTEXT, eMailInput.text.toString())
            Log.d(CONTEXT, passInput.text.toString())
            loginPresenter.sendIDtoInteractor(eMailInput.text.toString(),
                passInput.text.toString())
        }

        signUpCTA.setOnClickListener {
            goToPortal()
        }
    }

    override fun showError(errorMessage: String?) {
        this@MainActivity.runOnUiThread {
            val toast = Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG)
            toast.show()
        }
    }

    override fun goToHome(username: String, userToken: String) {
        val intent = Intent(this@MainActivity, HomePageActivity::class.java)
        finish()
        startActivity(intent)
    }

    override fun goToPortal() {
        val intent = Intent(this@MainActivity, PortailActivity::class.java)
        startActivity(intent)
    }

    override fun getSessionID() {
        userPrefs = this.getSharedPreferences("USER_PREF", Context.MODE_PRIVATE)
        val userMail = userPrefs.getString("userEmail", "")
        val userPass = userPrefs.getString("userPassword", "")

        if (!userMail.isNullOrEmpty() && !userPass.isNullOrEmpty())
            loginPresenter.sendIDtoInteractor(userMail, userPass)
    }

    override fun fillUserPref(userMail: String, userPass: String, userToken: String) {
        userPrefs = this.getSharedPreferences("USER_PREF", Context.MODE_PRIVATE)
        with(userPrefs.edit()) {
            putString("userEmail", userMail)
            putString("userPassword", userPass)
            apply()
        }
        val tokenPrefs = this.getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
        Log.d("USER_TOKEN", userToken)
        with(tokenPrefs.edit()) {
            putString("token", userToken)
            putString("user", userMail)
            apply()
        }
    }

    override fun onDestroy() {
        loginPresenter.onDestroy()
        super.onDestroy()
    }

    companion object {
        val CONTEXT = "LOGIN_ACTIVITY"
    }
}
