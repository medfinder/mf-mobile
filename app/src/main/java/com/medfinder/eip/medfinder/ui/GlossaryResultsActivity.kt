package com.medfinder.eip.medfinder.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.medfinder.eip.medfinder.R

class GlossaryResultsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_glossary_results)
    }
}
