package com.medfinder.eip.medfinder.model

import android.content.Context
import android.util.Log
import com.medfinder.eip.medfinder.Util
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class LoginInteractor(val context: Context) {
    private var okhttpclient = OkHttpClient()
    val JSON = MediaType.parse("application/json; charset=utf-8")
    private lateinit var appContext: Context

    interface OnFinishedListener {
        fun onResultSuccess(username: String, userToken: String, userPass: String)
        fun onResultFailure(errorMessage: String)
    }

    fun sendConnexionRequest(onFinishedListener: OnFinishedListener, userMail: String, userPass: String) {

        appContext = context
        val url = "${Util.getProperty("host", appContext)}/api/login"

        val json = JSONObject()
            .put("password", userPass)
            .put("username", userMail)

        val requestBody = RequestBody.create(JSON, json.toString())

        val request = Request.Builder()
            .addHeader("Content-Type", "application/json")
            .url(url)
            .post(requestBody)
            .build()

        okhttpclient.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val data = response.body()?.string()
                Log.d("SIGNUPPATIENT", "reponse $data")
                if (data != "Bad Request") {
                    val jsonObj = JSONObject(data)
                    Log.d("SIGNUPPATIENT", "reponse $jsonObj")
                    if (jsonObj.get("success") != true)
                        onFinishedListener.onResultFailure(jsonObj.getString("reason"))
                    else
                        onFinishedListener.onResultSuccess(
                            jsonObj.getString("user"),
                            jsonObj.getString("access_token"),
                            userPass
                        )
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                Log.d("onFailure", "LOGIN " + e.message)
                onFinishedListener.onResultFailure("There was an error during the connexion")
            }
        })
    }
}